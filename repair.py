#!/usr/bin/python2

from PyQt4 import QtCore, QtGui
import os, sys, glob

import repair_ui, user_ui, resources_rc
import libmisc, libsystem
misc = libmisc.Misc()
block = libsystem.Block()
misc.CATCH = True
misc.ROOT_DIR = '/mnt/repair'

Wizard = QtGui.QWizard()
ui = repair_ui.Ui_Wizard()
ui.setupUi(Wizard)

class RepairerThread(QtCore.QThread):
    def __init__(self, parent):
        super(RepairerThread, self).__init__()
        self.parent = parent

    def run(self):
        try:
            misc.dir_create('/mnt/repair')

            disk = str(ui.DisksBox.currentText())
            device = str(ui.PartitionsBox.currentText())
            if block.mounted(device):
                self.emit(QtCore.SIGNAL('message'), 'Unmounting: %s\n' % device)
                block.unmount(device)
            self.emit(QtCore.SIGNAL('message'), 'Mounting: %s\n' % device)
            block.mount(device, '/mnt/repair')

            if '/boot\t' in misc.file_read('/mnt/repair/etc/fstab'):
                self.emit(QtCore.SIGNAL('message'), 'Mounting /boot\n')
                misc.system_chroot(('mount', '/boot'))
            if '/boot/efi\t' in misc.file_read('/mnt/repair/etc/fstab'):
                self.emit(QtCore.SIGNAL('message'), 'Mounting /boot/efi\n')
                misc.system_chroot(('mount', '/boot/efi'))

            grub_bin = misc.whereis('grub-install', False, True)
            grubconf_bin = misc.whereis('grub-mkconfig', False, True)
            if grub_bin and grubconf_bin:
                self.emit(QtCore.SIGNAL('message'), 'Attempting GRUB repair\n')
                misc.dir_create('/mnt/repair/boot/grub')
                misc.system_chroot((grub_bin, '--force', disk))
                misc.system_chroot((grubconf_bin, '-o', '/boot/grub/grub.conf'))

            # FIXME: syslinux, (U)EFI bootloaders, etc.

            self.emit(QtCore.SIGNAL('message'), 'Success!\n')
        except SystemExit:
            # HACK!!! ignore system exit calls
            pass
        except Exception as detail:
            self.emit(QtCore.SIGNAL('failed'), str(detail))

class Repairer(object):
    def __init__(self):
        self.termprocess = None
        self.termcontainer = QtGui.QX11EmbedContainer()
        ui.TerminalWidget.setLayout(QtGui.QGridLayout())
        ui.TerminalWidget.layout().addWidget(self.termcontainer)
        self.thread = None

    def closeWizard(self):
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Are you sure you want to quit?', QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            if self.termprocess:
                self.termprocess.terminate()
            if self.thread:
                self.thread.quit()
            Wizard.reject()

    def showTerminal(self):
        # FIXME: xterm does not report fail for subprocess (the executed program)
        if self.termprocess and self.termprocess.state() == 2:
            return
        try:
            self.termprocess = QtCore.QProcess(self.termcontainer)
            args = ['-into', str(self.termcontainer.winId()), \
                '-e', sys.prefix + '/share/live-tools/chroot.sh /mnt/repair']
            self.termprocess.start(misc.whereis('xterm'), args)
            self.termprocess.waitForStarted()
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def interactiveRepairDevice(self):
        try:
            device = str(ui.PartitionsBox.currentText())
            block.mount(device, '/mnt/repair')
            self.showTerminal()
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def automaticRepairDevice(self):
        ui.ProgressBar.setRange(0, 0)
        ui.ProgressWidget.clear()
        # to avoid destroying the thread asign it to self
        self.thread = RepairerThread(ui)
        self.thread.finished.connect(self.finishRepair)
        Wizard.connect(self.thread, QtCore.SIGNAL('message'), ui.ProgressWidget.insertPlainText)
        Wizard.connect(self.thread, QtCore.SIGNAL('failed'), self.failedRepair)
        self.thread.start()

    def finishRepair(self):
        self.thread = False
        Wizard.button(QtGui.QWizard.NextButton).setEnabled(True)
        ui.ProgressBar.setRange(0, 1)

    def failedRepair(self, message):
        QtGui.QMessageBox.critical(Wizard, 'Critical', str(message))
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Do you want to retry? You will be sent back to the initial step', \
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            Wizard.restart()

    def systemShutdown(self):
        try:
            misc.system_command((misc.whereis('poweroff')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def systemReboot(self):
        try:
            misc.system_command((misc.whereis('reboot')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def changeDisk(self, sindex):
        # FIXME: implement, if needed
        pass

    def changePartition(self, sindex):
        # FIXME: this is probably better done at initialization filtering the invalid devices
        valid = False
        device = str(ui.PartitionsBox.currentText())
        try:
            block.unmount(device)
            block.fsck(device, block.blkid(device, 'TYPE'))
            block.mount(device, '/mnt/repair')
            if os.path.isfile('/mnt/repair/etc/entropy-release'):
                valid = True
                # primitive architecture check, attempting to repair x86
                # installation from x86_64 Live CD/DVD will not end up well
                for path in ('/lib64', '/usr/lib64'):
                    if os.path.exists(path) \
                        and not os.path.exists('/mnt/repair%s'):
                        valid = False
            block.unmount('/mnt/repair')
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))
        Wizard.button(QtGui.QWizard.NextButton).setEnabled(True)
        if not valid:
            QtGui.QMessageBox.critical(Wizard, 'Critical', 'The selected device does not seem valid')
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)

    def handleRepairChoice(self):
        if ui.InteractiveChoice.isChecked():
            ui.AutomaticChoice.setChecked(False)
        elif ui.AutomaticChoice.isChecked():
            ui.InteractiveChoice.setChecked(False)

    def handlePageChange(self, page):
        if page == 1:
            if ui.InteractiveChoice.isChecked():
                ui.DisksBox.setEnabled(False)
            elif ui.AutomaticChoice.isChecked():
                ui.DisksBox.setEnabled(True)
        if page == 2:
            if not ui.InteractiveChoice.isChecked():
                return Wizard.next()
            self.interactiveRepairDevice()
        elif page == 3:
            if not ui.AutomaticChoice.isChecked():
                return Wizard.next()
            Wizard.button(QtGui.QWizard.BackButton).setEnabled(False)
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)
            self.automaticRepairDevice()

    def validatePageChange(self):
        if Wizard.currentPage() == 1:
            if ui.InteractiveChoice.isChecked() and not ui.PartitionsBox.currentText():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'No partition selected')
                return False
            elif ui.AutomaticChoice.isChecked() and not ui.DisksBox.currentText():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'No disk selected')
                return False
        return True

repairer = Repairer()

devices = sorted(glob.glob('/dev/[sfhv]d[a-z]'))
ui.DisksBox.addItems(devices)
devices = sorted(glob.glob('/dev/[sfhv]d[a-z][0-9]*'))
ui.PartitionsBox.addItems(devices)

Wizard.button(QtGui.QWizard.CancelButton).clicked.disconnect()
Wizard.button(QtGui.QWizard.CancelButton).clicked.connect(repairer.closeWizard)
Wizard.connect(ui.DisksBox, QtCore.SIGNAL('activated(QString)'), repairer.changeDisk)
Wizard.connect(ui.PartitionsBox, QtCore.SIGNAL('activated(QString)'), repairer.changePartition)
Wizard.currentIdChanged.connect(repairer.handlePageChange)
Wizard.validateCurrentPage = repairer.validatePageChange
ui.InteractiveChoice.toggled.connect(repairer.handleRepairChoice)
ui.ShutdownButton.clicked.connect(repairer.systemShutdown)
ui.RebootButton.clicked.connect(repairer.systemReboot)

Wizard.show()

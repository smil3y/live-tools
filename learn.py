#!/usr/bin/python2

from PyQt4 import QtCore, QtGui
import os, sys

import learn_ui, resources_rc
import libmisc
misc = libmisc.Misc()
misc.CATCH = True

Wizard = QtGui.QWizard()
ui = learn_ui.Ui_Wizard()
ui.setupUi(Wizard)


class Teacher(object):
    def __init__(self):
        # set to English first, in case something (encoding?) fails
        self.setManual('en_US')
        # FIXME: detect via system/application locales
        # self.setManual('bg_BG')

    def closeWizard(self):
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Are you sure you want to quit?', QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            Wizard.reject()

    def systemShutdown(self):
        try:
            misc.system_command((misc.whereis('poweroff')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def systemInstall(self):
        try:
            import install
            Wizard.close()
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def systemReboot(self):
        try:
            misc.system_command((misc.whereis('reboot')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def setManual(self, language):
        manual = 'manual-%s.html' % language
        try:
            content = misc.file_read('%s/share/live-tools/%s' \
                % (sys.prefix, manual))
            ui.webView.setHtml(content)
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def handlePageChange(self, page):
        if page == 0:
            return
            Wizard.currentPage().setButtonText(Wizard.NextButton, 'Install')
            Wizard.button(QtGui.QWizard.BackButton).setEnabled(False)
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)

    def validatePageChange(self):
        if Wizard.currentId() == 1:
            pass
        return True

teacher = Teacher()

Wizard.button(QtGui.QWizard.CancelButton).clicked.disconnect()
Wizard.button(QtGui.QWizard.CancelButton).clicked.connect(teacher.closeWizard)
Wizard.currentIdChanged.connect(teacher.handlePageChange)
Wizard.validateCurrentPage = teacher.validatePageChange
ui.ShutdownButton.clicked.connect(teacher.systemShutdown)
ui.InstallButton.clicked.connect(teacher.systemInstall)
ui.RebootButton.clicked.connect(teacher.systemReboot)

Wizard.show()

#!/usr/bin/python2

from PyQt4 import QtCore, QtGui
import os, sys, glob, shutil, grp, random, hashlib

import install_ui, user_ui, resources_rc
import libmisc, libsystem
misc = libmisc.Misc()
block = libsystem.Block()
misc.CATCH = True
misc.ROOT_DIR = '/mnt/install'

Wizard = QtGui.QWizard()
Dialog = QtGui.QDialog()
ui = install_ui.Ui_Wizard()
ui.setupUi(Wizard)
user_ui = user_ui.Ui_Dialog()
user_ui.setupUi(Dialog)

def rcGet(conf, key, array=False):
    value = None
    for line in misc.file_read(conf).splitlines():
        if line.startswith(key):
            value = line.lstrip('%s=' % key)
            if array:
                value = value.replace('()', '')
            value = value.replace('"', '')
    return value

def rcSet(conf, key, value, array=False):
    if array:
        misc.file_substitute('\n%s=\(.*\n' % key, '\n%s=(%s)\n' % (key, value), \
            '/mnt/install/%s' % conf)
    else:
        misc.file_substitute('\n%s=.*\n' % key, '\n%s="%s"\n' % (key, value), \
            '/mnt/install/%s' % conf)

class InstallerThread(QtCore.QThread):
    def __init__(self, parent):
        super(InstallerThread, self).__init__()
        self.parent = parent

    def prepare_device(self, device, stype, mpoint, check):
        if block.mounted(device):
            self.emit(QtCore.SIGNAL('message'), 'Unmounting: %s\n' % device)
            block.unmount(device)
        if check:
            self.emit(QtCore.SIGNAL('message'), 'Checking filesystem on: %s\n' % device)
            block.fsck(device, stype)
        else:
            self.emit(QtCore.SIGNAL('message'), 'Creating %s filesystem on: %s\n' % (stype, device))
            block.mkfs(device, stype)
        if stype == 'swap':
            return
        self.emit(QtCore.SIGNAL('message'), 'Mounting %s to: %s\n' % (device, '/mnt/install' + mpoint))
        block.mount(device, '/mnt/install' + mpoint)

    def run(self):
        try:
            misc.dir_create('/mnt/install')

            # cache binaries paths and fail as early as possible if not found
            unsquash_bin = misc.whereis('unsquashfs')
            rc_update_bin = misc.whereis('rc-update')

            # build a dictionary for partitions layout to avoid parsing the
            # table over and over again
            layout = {}
            for irow in range(ui.LayoutTable.rowCount()):
                device = str(ui.LayoutTable.item(irow, 0).text())
                stype = str(ui.LayoutTable.cellWidget(irow, 1).currentText())
                mpoint = str(ui.LayoutTable.cellWidget(irow, 2).currentText())
                check = False
                if stype == 'keep':
                    check = True
                    stype = block.blkid(device, 'TYPE')
                # not interested in devices without mount points
                if not mpoint:
                    continue
                layout[device] = {'type': stype,
                                  'mount': mpoint,
                                  'check': check}

            # handle root first, everything else after
            for device in layout.keys():
                stype = layout[device]['type']
                mpoint = layout[device]['mount']
                check = layout[device]['check']
                if not mpoint == '/':
                    continue
                self.prepare_device(device, stype, mpoint, check)
            for device in layout.keys():
                stype = layout[device]['type']
                mpoint = layout[device]['mount']
                check = layout[device]['check']
                if mpoint == '/':
                    continue
                self.prepare_device(device, stype, mpoint, check)

            self.emit(QtCore.SIGNAL('message'), 'Unsquashing root filesystem, this will take a while...\n')
            misc.system_command((unsquash_bin, '-da', '32', '-fr', '32', \
                '-f', '-d', '/mnt/install', '/live/boot/live/root.sfs'))

            self.emit(QtCore.SIGNAL('message'), 'Creating static filesystems information file (fstab)\n')
            misc.file_write('/mnt/install/etc/fstab', '''# /etc/fstab: static file system information.
#
# <file system>\t<mount point>\t<type>\t<options>\t<dump>\t<pass>

devpts\t/dev/pts\tdevpts\tgid=5,mode=620\t0\t0
tmpfs\t/tmp\ttmpfs\tnodev,nosuid\t0\t0
tmpfs\t/run\ttmpfs\tdefaults\t0\t0
''')
            # ... and once more ...
            for device in layout.keys():
                stype = layout[device]['type']
                mpoint = layout[device]['mount']
                options = 'defaults'
                if mpoint == '/' and ui.ReadOnlyBox.isChecked():
                    options += ',ro'
                # for better boot reliability UUIDs are used
                uuid = block.blkid(device, 'UUID')
                misc.file_write('/mnt/install/etc/fstab', \
                    '\nUUID=%s\t%s\t%s\t%s\t0\t1' % (uuid, mpoint, stype, options), mode='a+')

            self.emit(QtCore.SIGNAL('message'), 'Removing Live CD/DVD/USB files\n')
            for sfile in ('etc/mkinitfs/files/live.conf', 'etc/mkinitfs/modules/live.conf', \
                'etc/mkinitfs/root/hooks/50_live', 'sbin/live-tools', \
                sys.prefix + '/share/live-tools', sys.prefix + '/share/live-tools', \
                sys.prefix + '/share/icons/hicolor/scalable/apps/live-tools.svg', \
                sys.prefix + '/share/applications/live-tools.desktop', 'lib/live'):
                sfull = '/mnt/install/%s' % sfile
                if os.path.isdir(sfull):
                    misc.dir_remove(sfull)
                elif os.path.exists(sfull):
                    os.unlink(sfull)
            for sfile in glob.glob('/mnt/install/etc/init.d/live_*'):
                os.unlink(sfile)
            misc.system_chroot((rc_update_bin, 'del', 'live_config', 'boot'))
            misc.system_chroot((rc_update_bin, 'del', 'live_eject', 'shutdown'))

            # cache executables from the new filesystem, it can be done from the
            # host too but meh..
            chpasswd_bin = misc.whereis('chpasswd', bchroot=True)
            adduser_bin = misc.whereis('adduser', bchroot=True)
            mkinitfs_bin = misc.whereis('mkinitfs', bchroot=True)
            grub_bin = misc.whereis('grub-install', bchroot=True)
            grubconf_bin = misc.whereis('grub-mkconfig', bchroot=True)

            self.emit(QtCore.SIGNAL('message'), 'Setting up Hostname\n')
            rcSet('/etc/conf.d/hostname', 'hostname', str(ui.Hostname.text()))

            self.emit(QtCore.SIGNAL('message'), 'Setting up Root user password\n')
            # NOTE: Busybox's chpasswd does not support -r argument so this has
            # to be done the hardway - by passing input to it in chroot
            password = str(ui.RootPassword.text())
            if ui.RootPasswordRandomize.isChecked():
                password = hashlib.sha512(str(random.random())).hexdigest()
            misc.system_chroot((chpasswd_bin), sinput='root:%s' % password)

            # NOTE: adduser is Busybox specifiec
            for irow in range(ui.UsersTable.rowCount()):
                name = str(ui.UsersTable.item(irow, 0).text())
                uid = str(ui.UsersTable.item(irow, 1).text())
                home = str(ui.UsersTable.item(irow, 2).text())
                groups = str(ui.UsersTable.item(irow, 3).text())
                system = ui.UsersTable.cellWidget(irow, 4).isChecked()
                password = str(ui.UsersTable.cellWidget(irow, 5).text())
                self.emit(QtCore.SIGNAL('message'), 'Adding regular user: %s\n' % name)
                adduser_command = [adduser_bin, '-D', '-u', uid, '-h', home]
                if system:
                    adduser_command.append('-S')
                adduser_command.append(name)
                misc.system_chroot((adduser_command))
                for group in groups.split(','):
                    # if groups is empty the split will produce ''
                    if not group:
                        continue
                    misc.system_chroot((adduser_bin, name, group))
                    misc.system_chroot((chpasswd_bin), sinput='%s:%s' % (name, password))

            self.emit(QtCore.SIGNAL('message'), 'Setting up Kernel modules\n')
            enabled = rcGet('/etc/conf.d/modules', 'modules')
            for irow in range(ui.ModulesTable.rowCount()):
                name = str(ui.ModulesTable.item(irow, 0).text())
                state = ui.ModulesTable.cellWidget(irow, 1).currentText()
                if state == 'blacklist':
                    misc.file_write('/mnt/install/etc/modprobe.d/%s.conf' % name, \
                        'blacklist %s\n' % name)
                elif state == 'boot':
                    enabled = '%s %s' % (name, enabled)
            rcSet('/etc/conf.d/modules', 'modules', enabled)

            self.emit(QtCore.SIGNAL('message'), 'Creating initial RAM filesystem\n')
            misc.system_chroot((mkinitfs_bin))

            grub_device = str(ui.BootloaderBox.currentText())
            if grub_device:
                self.emit(QtCore.SIGNAL('message'), 'Installing GRUB to: %s\n' % grub_device)
                misc.dir_create('/mnt/install/boot/grub')
                misc.system_chroot((grub_bin, '--force', grub_device))
                self.emit(QtCore.SIGNAL('message'), 'Creating GRUB configuration file\n')
                misc.system_chroot((grubconf_bin, '-o', '/boot/grub/grub.cfg'))
                # in some cases grub doesn't copy this file
                if not os.path.exists('/mnt/install/boot/grub/normal.mod'):
                    self.emit(QtCore.SIGNAL('message'), 'Copying GRUB normal.mod\n')
                    shutil.copy2('%s/lib/grub/i386-pc/normal.mod' % sys.prefix, \
                        '/mnt/install/boot/grub/normal.mod')

            for device in layout.keys():
                stype = layout[device]['type']
                mpoint = layout[device]['mount']
                check = layout[device]['check']
                self.emit(QtCore.SIGNAL('message'), 'Unmounting %s from: %s\n' % \
                    (device, '/mnt/install%s' % mpoint))
                block.unmount(device)

            self.emit(QtCore.SIGNAL('message'), 'Success!\n')
        except SystemExit:
            # HACK!!! ignore system exit calls
            pass
        except Exception as detail:
            self.emit(QtCore.SIGNAL('failed'), str(detail))

class Installer(object):
    def __init__(self):
        self.termprocess = None
        self.termcontainer = QtGui.QX11EmbedContainer()
        ui.TerminalWidget.setLayout(QtGui.QGridLayout())
        ui.TerminalWidget.layout().addWidget(self.termcontainer)
        self.thread = None

    def closeWizard(self):
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Are you sure you want to quit?', QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            if self.termprocess:
                self.termprocess.terminate()
            if self.thread:
                self.thread.quit()
            Wizard.reject()

    def showPartitioner(self, device):
        # FIXME: xterm does not report fail for subprocess (the executed program)
        if self.termprocess and self.termprocess.state() == 2:
            return
        try:
            self.termprocess = QtCore.QProcess(self.termcontainer)
            args = ['-into', str(self.termcontainer.winId()), \
                '-e', '%s %s' % (misc.whereis(str(ui.PartitionerBox.currentText())), device)]
            self.termprocess.start(misc.whereis('xterm'), args)
            self.termprocess.waitForStarted()
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def changeDisk(self, device):
        # force it to quit, termprocess is preserved on page change
        if self.termprocess:
            self.termprocess.terminate()
            self.termprocess.close()
        self.showPartitioner(ui.DisksBox.currentText())

    def adjustMountPoints(self, sindex):
        for irow in range(ui.LayoutTable.rowCount()):
            widget = ui.LayoutTable.cellWidget(irow, 2)
            if not widget.currentText() == sindex:
                index = widget.findText(sindex)
                if index:
                    widget.removeItem(index)
            # else enable type conditionally if a mount point is set
            elif sindex:
                # some filesystem restrictions
                ui.LayoutTable.cellWidget(irow, 1).setEnabled(False)
                if sindex == 'swap':
                    index = ui.LayoutTable.cellWidget(irow, 1).findText('swap')
                    ui.LayoutTable.cellWidget(irow, 1).setCurrentIndex(index)
                elif sindex == '/boot/efi':
                    index = ui.LayoutTable.cellWidget(irow, 1).findText('vfat')
                    ui.LayoutTable.cellWidget(irow, 1).setCurrentIndex(index)
                else:
                    device = str(ui.LayoutTable.item(irow, 0).text())
                    try:
                        stype = block.blkid(device, 'TYPE')
                    except Exception as detail:
                        QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))
                    if stype:
                        index = ui.LayoutTable.cellWidget(irow, 1).findText(stype)
                        ui.LayoutTable.cellWidget(irow, 1).setCurrentIndex(index)
                    ui.LayoutTable.cellWidget(irow, 1).setEnabled(True)
            else:
                ui.LayoutTable.cellWidget(irow, 1).setEnabled(False)

    def showLayout(self):
        devices = glob.glob('/dev/[sfhv]d[a-z][0-9]*')
        supported = ['keep'] # to indicate that format is not needed
        mountpoints = ('', '/', '/boot', '/home', '/tmp', '/usr', '/var', 'swap')
        for fstype in ('ext2', 'ext3', 'ext4', 'jfs', 'btrfs', 'ntfs'):
            if misc.whereis('mkfs.%s' % fstype, False):
                supported.append(fstype)
        if misc.whereis('mkfs.vfat', False):
            supported.append('vfat')
        if misc.whereis('mkswap', False):
            supported.append('swap')
        if 'vfat' in supported and os.path.exists('/sys/firmware/efi'):
            mountpoints = ('', '/', '/boot', '/boot/efi', '/home', '/tmp', '/usr', '/var', 'swap')
        ui.LayoutTable.clearContents()
        ui.LayoutTable.setRowCount(0)
        irow = 0
        for device in sorted(devices):
            TypeBox = QtGui.QComboBox(ui.LayoutTable)
            TypeBox.addItems(supported)
            TypeBox.setEnabled(False)
            stype = block.blkid(device, 'TYPE')
            if stype:
                index = TypeBox.findText(stype)
                TypeBox.setCurrentIndex(index)
            MountPointBox = QtGui.QComboBox(ui.LayoutTable)
            MountPointBox.addItems(mountpoints)
            Wizard.connect(MountPointBox, QtCore.SIGNAL('activated(QString)'), installer.adjustMountPoints)
            ui.LayoutTable.setRowCount(irow+1)
            ui.LayoutTable.setItem(irow, 0, QtGui.QTableWidgetItem(device))
            ui.LayoutTable.setCellWidget(irow, 1, TypeBox)
            ui.LayoutTable.setCellWidget(irow, 2, MountPointBox)
            irow += 1
        ui.LayoutTable.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)

    def showDetails(self):
        selected = ui.LayoutTable.selectedItems()
        if not selected:
            return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                'No device selected')
        device = selected[0].text()
        stype = block.blkid(device, 'TYPE') or 'Unknown'
        uuid = block.blkid(device, 'UUID') or ''
        label = block.blkid(device, 'LABEL') or ''
        QtGui.QMessageBox.information(Wizard, 'Information', \
            'Device: %s\nFilesystem: %s\nUUID: %s\nLABEL: %s\n' % \
            (device, stype, uuid, label))

    def showModules(self):
        irow = 0
        modorder = '/lib/modules/%s/modules.order' % os.uname()[2]
        for line in sorted(misc.file_read(modorder).splitlines()):
            ui.ModulesTable.setRowCount(irow+1)
            StateBox = QtGui.QComboBox()
            StateBox.addItems(('', 'boot', 'blacklist'))
            ui.ModulesTable.setItem(irow, 0, QtGui.QTableWidgetItem(misc.file_name(line)))
            ui.ModulesTable.setCellWidget(irow, 1, StateBox)
            irow += 1
        ui.ModulesTable.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)

    def handleUserButtons(self):
        if ui.UsersTable.selectedItems():
            ui.EditUserButton.setEnabled(True)
            ui.RemoveUserButton.setEnabled(True)
        else:
            ui.EditUserButton.setEnabled(False)
            ui.RemoveUserButton.setEnabled(False)

    def handleSystemUser(self):
        # FIXME: this should actually check /etc/login.defs, if it exists
        if user_ui.SystemBox.isChecked():
            user_ui.IDBox.setRange(100, 999)
        else:
            user_ui.IDBox.setRange(1000, 99999)

    def handleRootRandomize(self, state):
        if state:
            ui.RootPassword.setEnabled(False)
            ui.RootPassword2.setEnabled(False)
        else:
            ui.RootPassword.setEnabled(True)
            ui.RootPassword2.setEnabled(True)

    def prepareUser(self, setdefault=False):
        irow = 0
        default = ['tape', 'tty', 'floppy', 'disk', 'lp', 'dialout', 'audio']
        default.extend(('video', 'usb', 'cdrom', 'network', 'power', 'input'))
        default.extend(('scanner', 'users', 'sudo'))
        user_ui.GroupsTable.clearContents()
        for g in grp.getgrall():
            user_ui.GroupsTable.setRowCount(irow+1)
            ActiveBox = QtGui.QCheckBox()
            if setdefault and g.gr_name in default:
                ActiveBox.setChecked(True)
            user_ui.GroupsTable.setCellWidget(irow, 0, ActiveBox)
            user_ui.GroupsTable.setItem(irow, 1, QtGui.QTableWidgetItem(g.gr_name))
            irow += 1
        self.handleSystemUser()

    def addUser(self, edit=False):
        if not edit:
            self.prepareUser(True)
        rv = Dialog.exec_()
        if rv == 1:
            # sanity check fields
            if not str(user_ui.NameEdit.text()):
                return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                    'User name is required')
            elif not str(user_ui.HomeEdit.text()):
                # it is not really
                return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                    'Home directory is required')
            elif not str(user_ui.HomeEdit.text()).startswith('/'):
                return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                    'Home directory must be full path')
            elif not str(user_ui.PassEdit.text()) or not str(user_ui.PassEdit2.text()):
                # not required in all cases
                return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                    'User password is required')
            elif not str(user_ui.PassEdit.text()) == str(user_ui.PassEdit2.text()):
                return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                    'User passwords do not match')

            # sanity check with users table content
            if not edit:
                for irow in range(ui.UsersTable.rowCount()):
                    name = str(ui.UsersTable.item(irow, 0).text())
                    id = str(ui.UsersTable.item(irow, 1).text())
                    home = str(ui.UsersTable.item(irow, 2).text())
                    if name == str(user_ui.NameEdit.text()):
                        return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                            'User with that Name already added')
                    elif id == str(user_ui.IDBox.value()):
                        return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                            'User with that ID already added')
                    elif home == str(user_ui.HomeEdit.text()):
                        return QtGui.QMessageBox.critical(Wizard, 'Critical', \
                            'User with that Home already added')

            # convert groups to suitable variable
            groups = ''
            for irow in range(user_ui.GroupsTable.rowCount()):
                active = user_ui.GroupsTable.cellWidget(irow, 0).isChecked()
                if active:
                    name = str(user_ui.GroupsTable.item(irow, 1).text())
                    groups = '%s,%s' % (groups, name)
            groups = groups.lstrip(',')

            if edit:
                for itrow in range(ui.UsersTable.rowCount()):
                    name = str(ui.UsersTable.item(itrow, 0).text())
                    if name == str(user_ui.NameEdit.text()):
                        irow = itrow
            else:
                irow = ui.UsersTable.rowCount()
                ui.UsersTable.setRowCount(irow+1)

            SystemBox = QtGui.QCheckBox()
            SystemBox.setEnabled(False)
            SystemBox.setChecked(user_ui.SystemBox.isChecked())
            PasswordEdit = QtGui.QLineEdit()
            PasswordEdit.setEchoMode(QtGui.QLineEdit.Password)
            PasswordEdit.setReadOnly(True)
            PasswordEdit.setText(user_ui.PassEdit2.text())
            ui.UsersTable.setItem(irow, 0, QtGui.QTableWidgetItem(user_ui.NameEdit.text()))
            ui.UsersTable.setItem(irow, 1, QtGui.QTableWidgetItem(str(user_ui.IDBox.value())))
            ui.UsersTable.setItem(irow, 2, QtGui.QTableWidgetItem(user_ui.HomeEdit.text()))
            ui.UsersTable.setItem(irow, 3, QtGui.QTableWidgetItem(groups))
            ui.UsersTable.setCellWidget(irow, 4, SystemBox)
            ui.UsersTable.setCellWidget(irow, 5, PasswordEdit)

    def editUser(self):
        self.prepareUser()
        selected = ui.UsersTable.selectedItems()
        user_ui.NameEdit.setText(selected[0].text())
        user_ui.IDBox.setValue(int(selected[1].text()))
        user_ui.HomeEdit.setText(selected[2].text())
        for irow in range(user_ui.GroupsTable.rowCount()):
            for group in str(selected[3].text()).split(','):
                if group == str(user_ui.GroupsTable.item(irow, 1).text()):
                    user_ui.GroupsTable.cellWidget(irow, 0).setChecked(True)
        # NOTE: with cell widget selectedItems() does not return a item for
        # the widget so the actual column number is not used bellow to get the row
        user_ui.SystemBox.setChecked(ui.UsersTable.cellWidget(selected[3].row(), 4).isChecked())
        user_ui.PassEdit.setText(ui.UsersTable.cellWidget(selected[3].row(), 5).text())
        user_ui.PassEdit2.setText(ui.UsersTable.cellWidget(selected[3].row(), 5).text())
        self.addUser(True)

    def deleteUser(self):
        # FIXME: destroyed warning
        for item in ui.UsersTable.selectedItems():
            ui.UsersTable.removeRow(item.row())

    def startInstallation(self):
        ui.ProgressBar.setRange(0, 0)
        ui.ProgressWidget.clear()
        # to avoid destroying the thread asign it to self
        self.thread = InstallerThread(ui)
        self.thread.finished.connect(self.finishInstallation)
        Wizard.connect(self.thread, QtCore.SIGNAL('message'), ui.ProgressWidget.insertPlainText)
        Wizard.connect(self.thread, QtCore.SIGNAL('failed'), self.failedInstallation)
        self.thread.start()

    def failedInstallation(self, message):
        QtGui.QMessageBox.critical(Wizard, 'Critical', str(message))
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Do you want to retry? You will be sent back to the initial step', \
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            Wizard.restart()

    def finishInstallation(self):
        self.thread = False
        Wizard.button(QtGui.QWizard.NextButton).setEnabled(True)
        ui.ProgressBar.setRange(0, 1)

    def systemShutdown(self):
        try:
            misc.system_command((misc.whereis('poweroff')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def systemReboot(self):
        try:
            misc.system_command((misc.whereis('reboot')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def handlePageChange(self, page):
        if page == 0:
            ui.UsersTable.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)
        elif page == 1:
            if devices:
                installer.showPartitioner(devices[0])
        elif page == 2:
            # partprobe is part of parted, keeping it optional
            partprobe = misc.whereis('partprobe', False)
            # partx is part of util-linux
            partx = misc.whereis('partx', False)
            before = glob.glob('/dev/[sfhv]d[a-z][0-9]*')
            for disk in glob.glob('/dev/[sfhv]d[a-z]'):
                try:
                    if partprobe:
                        misc.system_command((partprobe, disk))
                    elif partx:
                        misc.system_command((partx, '-u', disk))
                    else:
                        misc.file_write('/sys/block/%s/device/rescan' % os.path.basename(disk), '1')
                except Exception as detail:
                    QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))
            after = glob.glob('/dev/[sfhv]d[a-z][0-9]*')
            if before == after and ui.LayoutTable.rowCount() > 0:
                # do not reset the layout
                return
            self.showLayout()
        elif page == 3:
            Wizard.currentPage().setButtonText(Wizard.NextButton, 'Install')
        elif page == 4:
            Wizard.button(QtGui.QWizard.BackButton).setEnabled(False)
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)
            self.startInstallation()

    def validatePageChange(self):
        if Wizard.currentId() == 0:
            if not ui.Hostname.text():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'Hostname is required')
                return False
            elif not ui.RootPassword.text() and not ui.RootPasswordRandomize.isChecked():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'Root password is required')
                return False
            elif not ui.RootPassword.text() == ui.RootPassword2.text():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'Root passwords do not match')
                ui.RootPassword.clear()
                ui.RootPassword2.clear()
                return False
        elif Wizard.currentId() == 2:
            found_root = False
            found_var = False
            found_efi = False
            for irow in range(ui.LayoutTable.rowCount()):
                mpoint = str(ui.LayoutTable.cellWidget(irow, 2).currentText())
                if mpoint == '/':
                    found_root = True
                elif mpoint == '/var':
                    found_var = True
                elif mpoint == '/boot/efi':
                    found_efi = True
            if not found_root:
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'Root filesystem is required')
                return False
            elif ui.ReadOnlyBox.isChecked() and not found_var:
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'Var filesystem is required')
                return False
            elif os.path.exists('/sys/firmware/efi') and not found_efi:
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'EFI filesystem is required')
                return False
        elif Wizard.currentId() == 3:
            answer = QtGui.QMessageBox.question(Wizard, 'Question', \
                'You are about to install the Live CD/DVD/USB on your ' \
                'system, the changes done during the process may kill your ' \
                'pony. Are you sure you want to continue?', \
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
            if not answer == QtGui.QMessageBox.Yes:
                return False
        return True

installer = Installer()


for program in ('cparted', 'cfdisk', 'gdisk', 'fdisk', 'parted'):
    if misc.whereis(program, False):
        ui.PartitionerBox.addItem(program)
devices = sorted(glob.glob('/dev/[sfhv]d[a-z]'))
ui.BootloaderBox.addItem('') # to make it optional
ui.BootloaderBox.addItems(devices)
ui.DisksBox.addItems(devices)
ui.Hostname.setText(rcGet('/etc/conf.d/hostname', 'hostname'))

Wizard.button(QtGui.QWizard.CancelButton).clicked.disconnect()
Wizard.button(QtGui.QWizard.CancelButton).clicked.connect(installer.closeWizard)
Wizard.connect(ui.DisksBox, QtCore.SIGNAL('activated(QString)'), installer.changeDisk)
Wizard.connect(ui.PartitionerBox, QtCore.SIGNAL('activated(QString)'), installer.changeDisk)
Wizard.currentIdChanged.connect(installer.handlePageChange)
Wizard.validateCurrentPage = installer.validatePageChange
ui.ResetButton.clicked.connect(installer.showLayout)
ui.DetailsButton.clicked.connect(installer.showDetails)
ui.RootPasswordRandomize.stateChanged.connect(installer.handleRootRandomize)
ui.UsersTable.itemSelectionChanged.connect(installer.handleUserButtons)
ui.AddUserButton.clicked.connect(installer.addUser)
ui.EditUserButton.clicked.connect(installer.editUser)
ui.RemoveUserButton.clicked.connect(installer.deleteUser)
user_ui.SystemBox.stateChanged.connect(installer.handleSystemUser)
ui.ShutdownButton.clicked.connect(installer.systemShutdown)
ui.RebootButton.clicked.connect(installer.systemReboot)

# to avoid hogging due to screen repaint on every item add doing it now while
# the tables are hidden
installer.showModules()

Wizard.show()

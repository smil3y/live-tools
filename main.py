#!/usr/bin/python2

from PyQt4 import QtCore, QtGui
import sys

sys.path.append(sys.prefix + '/share/live-tools')

import main_ui, resources_rc
import libmisc, libmessage
message = libmessage.Message()
misc = libmisc.Misc()
misc.CATCH = True

app = QtGui.QApplication(sys.argv)
icon = QtGui.QIcon()
icon.setThemeName('ariya')
MainWindow = QtGui.QMainWindow()
ui = main_ui.Ui_MainWindow()
ui.setupUi(MainWindow)

def run_upgrade():
    try:
        import upgrade
        MainWindow.close()
    except Exception as detail:
        QtGui.QMessageBox.critical(MainWindow, 'Critical', str(detail))

def run_install():
    try:
        import install
        MainWindow.close()
    except Exception as detail:
        QtGui.QMessageBox.critical(MainWindow, 'Critical', str(detail))

def run_repair():
    try:
        import repair
        MainWindow.close()
    except Exception as detail:
        QtGui.QMessageBox.critical(MainWindow, 'Critical', str(detail))

def run_learn():
    try:
        import learn
        MainWindow.close()
    except Exception as detail:
        QtGui.QMessageBox.critical(MainWindow, 'Critical', str(detail))

run_main = True
for arg in sys.argv:
    if arg == '--upgrade':
        run_main = False
        run_upgrade()
    elif arg == '--install':
        run_main = False
        run_install()
    elif arg == '--repair':
        run_main = False
        run_repair()
    elif arg == '--learn':
        run_main = False
        run_learn()

if run_main:
    ui.UpgradeButton.clicked.connect(run_upgrade)
    ui.InstallButton.clicked.connect(run_install)
    ui.RepairButton.clicked.connect(run_repair)
    ui.LearnButton.clicked.connect(run_learn)
    MainWindow.show()

sys.exit(app.exec_())
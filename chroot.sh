#!/bin/bash

WORK_DIR="$@"
COMMAND='/bin/bash'
Reset='\e[0m'
Red='\e[1;31m'
Green='\e[1;32m'
Yellow='\e[1;33m'

echo -e "${Yellow}* ${Green}Checking${Reset}"
if [ "$UID" != "0" ];then
    echo -e "  ${Red}=> ${Yellow}You are not root!${Reset}"
    exit 1
elif [ "$WORK_DIR" = "/" ] || [ ! -d "$WORK_DIR/etc" ] || [ ! -d "$WORK_DIR/root" ];then
    echo -e "${Red}=> ${Yellow}The path \"$WORK_DIR\" isn't usable root filesystem or has been corruped.${Reset}"
    exit 2
fi

echo -e "${Yellow}* ${Green}Doing some preparations${Reset}"
cp -f /etc/resolv.conf "$WORK_DIR/etc"

echo -e "${Yellow}* ${Green}Mounting${Reset}"
for dir in dev dev/pts dev/shm proc sys;do
    if [ -d "/$dir" ] && [ ! $(mountpoint -q "$WORK_DIR/$dir") ];then
        mkdir -p "$WORK_DIR/$dir"
        mount --bind "/$dir" "$WORK_DIR/$dir"
    fi
done

echo -e "${Yellow}* ${Green}Entering Chroot env.${Reset}"
if [ "$(uname -m)" = "x86_64" ] && [ ! -d "$WORK_DIR/lib64" ];then
    linux32 chroot "$WORK_DIR" env HOME=/root LC_ALL=C PS1="(chroot) $PS1" $COMMAND
else
    chroot "$WORK_DIR" $COMMAND
fi

echo -e "${Yellow}* ${Green}Unmounting pseudo filesystems${Reset}"
for dir in dev dev/pts dev/shm proc sys;do
    if mountpoint -q "$WORK_DIR/$dir" ;then
        umount -fl "$WORK_DIR/$dir"
    fi
done


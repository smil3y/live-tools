#!/usr/bin/python2

from PyQt4 import QtCore, QtGui
import os, sys, glob, shutil

import upgrade_ui, user_ui, resources_rc
import libmisc, libsystem, libpackage, libspm
misc = libmisc.Misc()
block = libsystem.Block()
database = libpackage.Database()
misc.CATCH = True
misc.ROOT_DIR = '/mnt/upgrade'
database.ROOT_DIR = '/mnt/upgrade'
database.LOCAL_DIR = '/mnt/upgrade/var/local/spm'
database.CACHE_DIR = '/mnt/upgrade/var/cache/spm'
libspm.TRIGGERS = True
libspm.ROOT_DIR = '/mnt/upgrade'
libspm.CACHE_DIR = '/mnt/upgrade/var/cache/spm'

Wizard = QtGui.QWizard()
ui = upgrade_ui.Ui_Wizard()
ui.setupUi(Wizard)

class UpgraderThread(QtCore.QThread):
    def __init__(self, parent):
        super(UpgraderThread, self).__init__()
        self.parent = parent

    def prepare_device(self, device, stype, mpoint):
        if block.mounted(device):
            self.emit(QtCore.SIGNAL('message'), 'Unmounting: %s\n' % device)
            block.unmount(device)
        self.emit(QtCore.SIGNAL('message'), 'Checking filesystem on: %s\n' % device)
        block.fsck(device, block.blkid(device, 'TYPE'))
        self.emit(QtCore.SIGNAL('message'), 'Mounting %s to: %s\n' % (device, '/mnt/upgrade' + mpoint))
        block.mount(device, '/mnt/upgrade' + mpoint)

    def run(self):
        try:
            misc.dir_create('/mnt/upgrade')

            # cache binaries paths and fail as early as possible if not found
            unsquash_bin = misc.whereis('unsquashfs')
            rc_update_bin = misc.whereis('rc-update')

            disk = str(ui.DisksBox.currentText())
            device = str(ui.PartitionsBox.currentText())
            if block.mounted(device):
                self.emit(QtCore.SIGNAL('message'), 'Unmounting: %s\n' % device)
                block.unmount(device)
            self.emit(QtCore.SIGNAL('message'), 'Mounting: %s\n' % device)
            block.mount(device, '/mnt/upgrade')

            self.emit(QtCore.SIGNAL('message'), 'Parsing static filesystems file (fstab)\n')
            dfstab = {}
            for line in misc.file_read('/mnt/upgrade/etc/fstab').splitlines():
                line = line.split('\t')
                device = line[0]
                if not device.startswith(('/dev', 'UUID=', 'LABEL=')):
                    # FIXME: network filesystems, root over NFS anyone?
                    continue
                # normalize, get the /dev node
                elif device.startswith('UUID='):
                    device = '/dev/disk/by-uuid/%s' % device.split('=')[1]
                elif device.startswith('LABEL='):
                    device = '/dev/disk/by-label/%s' % device.split('=')[1]

                mpoint = line[1]
                stype = block.blkid(device, 'TYPE')
                if stype == 'swap':
                    continue
                dfstab[device] = {
                    'mount': mpoint,
                    'type': stype,
                }

            for device in dfstab:
                stype = dfstab[device]['type']
                mpoint = dfstab[device]['mount']
                # skip root (/), already prepared
                if mpoint == '/':
                    continue
                self.prepare_device(device, stype, mpoint)

            if misc.url_ping():
                self.emit(QtCore.SIGNAL('message'), 'Syncronizing repositories\n')
                m = libspm.Repo(libspm.REPOSITORIES, do_sync=True, do_prune=True)
                m.main()

            self.emit(QtCore.SIGNAL('message'), 'Collecting information\n')
            # NOTE: first four lines are information or null output, not actual enties.
            # for maximim reliability (fool proof) it would be better to just loop mount
            # the squashfs and list all files/links/directories but that would be way
            # too slow (I think)
            lrootfiles = misc.system_communicate((unsquash_bin, '-da', '32', '-fr', '32', \
                '-l', '-d', '', '/live/boot/live/root.sfs')).splitlines()[4:]
            lsystemfiles = []
            lbackups = []
            # collect system and backup files
            for starget in database.local_all():
                lbackups.extend(database.remote_metadata(starget, 'backup'))
                # filter software not installed on new root
                if not starget.replace('/mnt/upgrade', '') + '/metadata.json' in lrootfiles:
                    continue
                lsystemfiles.extend(database.local_metadata(starget, 'footprint'))

            self.emit(QtCore.SIGNAL('message'), 'Backing up files/directories...\n')
            for sfile in lbackups:
                sfull = '/mnt/upgrade/%s' % sfile
                if os.path.exists(sfull):
                    shutil.copy2(sfull, sfull + '.backup')

            self.emit(QtCore.SIGNAL('message'), 'Unsquashing root filesystem, this will take a while...\n')
            misc.system_command((unsquash_bin, '-da', '32', '-fr', '32', \
                '-f', '-d', '/mnt/upgrade', '/live/boot/live/root.sfs'))

            self.emit(QtCore.SIGNAL('message'), 'Removing Live CD/DVD/USB files\n')
            for sfile in ('etc/mkinitfs/files/live.conf', 'etc/mkinitfs/modules/live.conf', \
                'etc/mkinitfs/root/hooks/live', 'sbin/live-tools', \
                sys.prefix + '/share/live-tools', sys.prefix + '/share/live-tools', \
                sys.prefix + '/share/icons/hicolor/scalable/apps/live-tools.svg', \
                sys.prefix + '/share/applications/live-tools.desktop'):
                sfull = '/mnt/upgrade/%s' % sfile
                if os.path.isdir(sfull):
                    misc.dir_remove(sfull)
                elif os.path.exists(sfull):
                    os.unlink(sfull)
            for sfile in glob.glob('/mnt/install/etc/init.d/live_*'):
                os.unlink(sfile)
            misc.system_chroot((rc_update_bin, 'del', 'live_config', 'boot'))
            misc.system_chroot((rc_update_bin, 'del', 'live_eject', 'shutdown'))

            self.emit(QtCore.SIGNAL('message'), 'Removing obsolete files/directories...\n')
            for sfile in lsystemfiles:
                sfull = '/mnt/upgrade%s' % sfile
                if not sfile in lrootfiles and os.path.exists(sfull):
                    if os.path.isdir(sfull):
                        misc.dir_remove(sfull)
                    else:
                        os.unlink(sfull)

            self.emit(QtCore.SIGNAL('message'), 'Restoring original files/directories...\n')
            for sfile in lbackups:
                # FIXME: attempt merge instead of overwrite
                sfull = '/mnt/upgrade/%s' % sfile
                sbackup = '%s.backup' % sfull
                if os.path.exists(sbackup):
                    shutil.copy2(sbackup, sfull)

            self.emit(QtCore.SIGNAL('message'), 'Updating common caches\n')
            m = libspm.Source('bash')
            m.post_update_databases(lrootfiles, 'merge')

            disk = str(ui.DisksBox.currentText())
            if disk:
                grub_bin = misc.whereis('grub-install', bchroot=True)
                grubconf_bin = misc.whereis('grub-mkconfig', bchroot=True)
                self.emit(QtCore.SIGNAL('message'), 'Installing GRUB to: %s\n' % disk)
                misc.dir_create('/mnt/upgrade/boot/grub')
                misc.system_chroot((grub_bin, '--force', disk))
                self.emit(QtCore.SIGNAL('message'), 'Creating GRUB configuration file\n')
                misc.system_chroot((grubconf_bin, '-o', '/boot/grub/grub.cfg'))
                # in some cases grub doesn't copy this file
                if not os.path.exists('/mnt/upgrade/boot/grub/normal.mod'):
                    self.emit(QtCore.SIGNAL('message'), 'Copying GRUB normal.mod\n')
                    shutil.copy2('%s/lib/grub/i386-pc/normal.mod' % sys.prefix, \
                        '/mnt/upgrade/boot/grub/normal.mod')

            for device in dfstab:
                stype = dfstab[device]['type']
                mpoint = dfstab[device]['mount']
                self.emit(QtCore.SIGNAL('message'), 'Unmounting: %s\n' % device)
                block.unmount(device)

            self.emit(QtCore.SIGNAL('message'), 'Success!\n')
        except SystemExit:
            # HACK!!! ignore system exit calls
            pass
        except Exception as detail:
            self.emit(QtCore.SIGNAL('failed'), str(detail))

class Upgrader(object):
    def __init__(self):
        self.thread = None

    def closeWizard(self):
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Are you sure you want to quit?', QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            if self.thread:
                self.thread.quit()
            Wizard.reject()

    def UpgradeDevice(self):
        ui.ProgressBar.setRange(0, 0)
        ui.ProgressWidget.clear()
        # to avoid destroying the thread asign it to self
        self.thread = UpgraderThread(ui)
        self.thread.finished.connect(self.finishUpgrade)
        Wizard.connect(self.thread, QtCore.SIGNAL('message'), ui.ProgressWidget.insertPlainText)
        Wizard.connect(self.thread, QtCore.SIGNAL('failed'), self.failedUpgrade)
        self.thread.start()

    def finishUpgrade(self):
        self.thread = False
        Wizard.button(QtGui.QWizard.NextButton).setEnabled(True)
        ui.ProgressBar.setRange(0, 1)

    def failedUpgrade(self, message):
        QtGui.QMessageBox.critical(Wizard, 'Critical', str(message))
        answer = QtGui.QMessageBox.question(Wizard, 'Question', \
            'Do you want to retry? You will be sent back to the initial step', \
            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if answer == QtGui.QMessageBox.Yes:
            Wizard.restart()

    def systemShutdown(self):
        try:
            misc.system_command((misc.whereis('poweroff')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def systemReboot(self):
        try:
            misc.system_command((misc.whereis('reboot')))
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))

    def changeDisk(self, sindex):
        # FIXME: implement, if needed
        pass

    def changePartition(self, sindex):
        # FIXME: this is probably better done at initialization filtering the invalid devices
        valid = False
        device = str(ui.PartitionsBox.currentText())
        try:
            block.unmount(device)
            misc.system_command((misc.whereis('fsck'), device))
            block.mount(device, '/mnt/upgrade')
            if os.path.isfile('/mnt/upgrade/etc/entropy-release'):
                valid = True
                # primitive architecture check, attempting to update x86
                # installation from x86_64 Live CD/DVD will not end up well
                for path in ('/lib64', '/usr/lib64'):
                    if os.path.exists(path) \
                        and not os.path.exists('/mnt/upgrade%s'):
                        valid = False
            block.unmount('/mnt/upgrade')
        except Exception as detail:
            QtGui.QMessageBox.critical(Wizard, 'Critical', str(detail))
        Wizard.button(QtGui.QWizard.NextButton).setEnabled(True)
        if not valid:
            QtGui.QMessageBox.critical(Wizard, 'Critical', 'The selected device does not seem valid')
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)

    def handlePageChange(self, page):
        if page == 1:
            Wizard.button(QtGui.QWizard.BackButton).setEnabled(False)
            Wizard.button(QtGui.QWizard.NextButton).setEnabled(False)
            self.UpgradeDevice()

    def validatePageChange(self):
        if Wizard.currentPage() == 0:
            if ui.InteractiveChoice.isChecked() and not ui.PartitionsBox.currentText():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'No partition selected')
                return False
            elif ui.AutomaticChoice.isChecked() and not ui.DisksBox.currentText():
                QtGui.QMessageBox.critical(Wizard, 'Critical', 'No disk selected')
                return False
        return True

upgrader = Upgrader()

devices = sorted(glob.glob('/dev/[sfhv]d[a-z]'))
ui.DisksBox.addItems(devices)
devices = sorted(glob.glob('/dev/[sfhv]d[a-z][0-9]*'))
ui.PartitionsBox.addItems(devices)

Wizard.button(QtGui.QWizard.CancelButton).clicked.disconnect()
Wizard.button(QtGui.QWizard.CancelButton).clicked.connect(upgrader.closeWizard)
Wizard.connect(ui.DisksBox, QtCore.SIGNAL('activated(QString)'), upgrader.changeDisk)
Wizard.connect(ui.PartitionsBox, QtCore.SIGNAL('activated(QString)'), upgrader.changePartition)
Wizard.currentIdChanged.connect(upgrader.handlePageChange)
Wizard.validateCurrentPage = upgrader.validatePageChange
ui.ShutdownButton.clicked.connect(upgrader.systemShutdown)
ui.RebootButton.clicked.connect(upgrader.systemReboot)

Wizard.show()

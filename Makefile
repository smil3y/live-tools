DESTDIR = 
PREFIX = /usr

all: clean
	pyrcc4 resources.qrc -o resources_rc.py
	pyuic4 upgrade.ui -o upgrade_ui.py
	pyuic4 install.ui -o install_ui.py
	pyuic4 user.ui -o user_ui.py
	pyuic4 repair.ui -o repair_ui.py
	pyuic4 learn.ui -o learn_ui.py
	pyuic4 main.ui -o main_ui.py

install:
	install -vd $(DESTDIR)$(PREFIX)/sbin \
		$(DESTDIR)$(PREFIX)/share/live-tools/ \
		$(DESTDIR)$(PREFIX)/share/applications/ \
		$(DESTDIR)$(PREFIX)/share/icons/hicolor/scalable/apps/
	install -v main.py $(DESTDIR)$(PREFIX)/sbin/live-tools
	install -v install.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v upgrade.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v repair.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v learn.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v *_rc.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v *_ui.py $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v chroot.sh $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v congrats.png $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v *.html $(DESTDIR)$(PREFIX)/share/live-tools/
	install -v live-tools.desktop \
		$(DESTDIR)$(PREFIX)/share/applications/live-tools.desktop
	install -v live-tools.svg \
		$(DESTDIR)$(PREFIX)/share/icons/hicolor/scalable/apps/live-tools.svg

uninstall:
	rm -vf $(DESTDIR)$(PREFIX)/sbin/live-tools
	rm -vrf $(DESTDIR)$(PREFIX)/share/live-tools/
	rm -vf $(DESTDIR)$(PREFIX)/share/applications/live-tools.desktop
	rm -vf $(DESTDIR)$(PREFIX)/share/icons/hicolor/scalable/apps/live-tools.svg

clean:
	rm -vf *_ui.py *_rc.py *.pyc
run:
	python main.py

.PHONY: all install uninstall clean run